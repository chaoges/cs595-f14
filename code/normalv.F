c CHANGES: **pf
c -changed maxlen to be a macro. Replaced all 0:9600 w/ 0:MAXLEN
c -must pass through the preprocessor before compiling
c
#include "defineMaxlen.h"
c#define MAXLEN 34000
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c HELE SHAW. 2/28/93  **[0,2PI]**
c
c
c
c    THIS CONTAINS USET AND THE VELOCITY ROUTINE .
c    AND RNTVEL, which computes the normal velocity.
c
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c  THIS CODE COMPUTES THE NORMAL VELOCITY GIVEN THE U,V FROM THE
c  VELOCITY ROUTINE
c
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine rntvel(m,h,p2,x,y,sl,theta,u,v,un,ut,dcut)
       implicit double precision (a-h,o-z)
       dimension x(0:MAXLEN),y(0:MAXLEN)
       dimension u(0:MAXLEN),v(0:MAXLEN),un(0:MAXLEN),ut(0:MAXLEN)
       dimension sx(0:MAXLEN),sy(0:MAXLEN),theta(0:MAXLEN)

c       call kfilter(m,dcut,u)
c       call kfilter(m,dcut,v)

C       write(22,*)'sx=, sy,sl',sl
       do j=0,m-1

        sx(j)=cos(theta(j))
        sy(j)=sin(theta(j))
C         write(22,*)sx(j),sy(j)
 
       end do


c       call kfilter(m,dcut,sx)
c       call kfilter(m,dcut,sy)
c       call kfilter(m,dcut,u)
c       call kfilter(m,dcut,v)

       do j=0,m-1

        un(j)=(sx(j)*v(j)-sy(j)*u(j))
        ut(j)=(sx(j)*u(j)+sy(j)*v(j))

       end do
       un(m)=un(0)
       ut(m)=ut(0)

c       call kfilter(m,dcut,un)
c       call kfilter(m,dcut,ut)
 
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c   THIS CODE FORMS THE NORMAL AND TAN VELOCITY GIVEN THE ANGLE AND ARCLENGTH 
c   and returns the normal and tan vel. from the ave. vel. integral
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine uset(nintf,m,h,p2,del,dcut,sl,theta,dalpha,w,un,ut,
     &                                               x0,y0,ag,tau)
        implicit double precision (a-h,o-z)
        dimension theta(0:MAXLEN,10),un(0:MAXLEN,10)
        dimension ut(0:MAXLEN,10),x(0:MAXLEN,10)
        dimension y(0:MAXLEN,10),u(0:MAXLEN,10),v(0:MAXLEN,10),w(0:MAXLEN,10)
        dimension dalpha(0:MAXLEN,10)
        dimension wt(0:MAXLEN,10),dkap(0:MAXLEN,10),sdkap(0:MAXLEN,10)
        dimension sl(10),x0(10),y0(10)

c  compute vortex sheet strength for Hele-Shaw flows


        call acurvmatr(nintf,m,sl,theta,dkap,dcut,h)
        call fd1matr(nintf,m,dkap,sdkap,h,dcut)

        do jj=1,nintf
        do j=0,m-1
         wt(j,jj)=tau*sdkap(j,jj)+ag*sl(jj)*dsin(theta(j,jj))
         end do
         wt(m,jj)=wt(0,jj)
        end do

c  compute velocities

        call reconmatr(nintf,m,h,p2,sl,theta,x,y,avx,avy,dcut,x0,y0)
        call velocitymatr(nintf,m,h,p2,x,y,wt,u,v,del,dcut)
        call rntvelmatr(nintf,m,h,p2,x,y,sl,theta,u,v,un,ut,dcut)

      return
      end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c   THIS FILE CONTAINS THE VELOCITY EVALUATION FOR H-S
c
cccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c  Subroutine to compute the velocity vector
c  THIS IS FOR CLOSED INTERFACES.
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       The subroutine to calculate the velocity integral.
c
      subroutine velocity(N,h,p2,xc,yc,w,u,v,del,dcut)
      implicit double precision(a-h,o-z),integer(i-n)
      dimension xc(0:MAXLEN), u(0:MAXLEN)
      dimension yc(0:MAXLEN), v(0:MAXLEN), w(0:MAXLEN)
      dimension xct(0:MAXLEN), yct(0:MAXLEN), wt(0:MAXLEN)
      integer Nh,N,i,j,in
      pi=4.d0*datan(1.d0)
      p2=2.d0*pi
      Nh=N/2
c      call kfilterx(N,dcut,xc,h)
c      call kfilter(N,dcut,yc)
c      call kfilter(N,dcut,w)
      do 100 j=0,N-1
      xct(j)=xc(j)
      yct(j)=yc(j)
      wt(j)=w(j)
 100  continue

      do 200 j=1,N+2
      xct(N+j-1)=xc(j-1)
      yct(N+j-1)=yc(j-1)
      wt(N+j-1)=w(j-1)
      u(j-1)=0.d0
      v(j-1)=0.d0
 200  continue
      do 210 j=0,Nh-1
      do 220 i=0,N-1
      in=i+2*j+1
 
c      temp= dcosh(p2*(yct(i)-yct(in)))
c     $   -dcos(p2*(xct(i)-xct(in))) + del*del
c       
c      u(i)=u(i)-wt(in)*dsinh(p2*(yct(i)-yct(in)))/temp
c 
c      v(i)=v(i)+wt(in)*dsin(p2*(xct(i)-xct(in)))/temp

       temp=(yct(i)-yct(in))**2 + (xct(i)-xct(in))**2 + del*del

       u(i)=u(i)-wt(in)*(yct(i)-yct(in))/temp

       v(i)=v(i)+wt(in)*(xct(i)-xct(in))/temp
 
 220  continue
 210  continue
      do 223 i=0,N-1
      u(i)=u(i)*2.d0*h/p2
      v(i)=v(i)*2.d0*h/p2
 223  continue
      u(N)=u(0)
      v(N)=v(0)
c      call kfilter(N,dcut,u)
c      call kfilter(N,dcut,v)
      return
      end
 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c   THIS FILE CONTAINS THE VELOCITY EVALUATION FOR H-S
c   USING MULTIPLE INTEFACES!!!
c
cccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c  Subroutine to compute the velocity vector
c  THIS IS FOR CLOSED INTERFACES.
c
c   MULTIPLE INTERFACE VERSION!!
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       The subroutine to calculate the velocity integral.
c
      subroutine velocitymatr(nintf,N,h,p2,xc,yc,w,u,v,del,dcut)
      implicit double precision(a-h,o-z),integer(i-n)
      dimension xc(0:MAXLEN,10), u(0:MAXLEN,10)
      dimension yc(0:MAXLEN,10), v(0:MAXLEN,10), w(0:MAXLEN,10)
      dimension xct(0:MAXLEN,10), yct(0:MAXLEN,10), wt(0:MAXLEN,10)
      integer Nh,N,i,j,in
      pi=4.d0*datan(1.d0)
      p2=2.d0*pi
      Nh=N/2
c      call kfilterx(N,dcut,xc,h)
c      call kfilter(N,dcut,yc)
c      call kfilter(N,dcut,w)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c  DO THE VELOCITY ROUTINE FOR EACH INTERFACE.
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      do jj=1,nintf
       do j=0,N-1
        xct(j,jj)=xc(j,jj)
        yct(j,jj)=yc(j,jj)
        wt(j,jj)=w(j,jj)
       end do
      end do

      do jj=1,nintf
       do j=1,N+2
         xct(N+j-1,jj)=xc(j-1,jj)
         yct(N+j-1,jj)=yc(j-1,jj)
         wt(N+j-1,jj)=w(j-1,jj)
         u(j-1,jj)=0.d0
         v(j-1,jj)=0.d0
       end do
      end do

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c compute the velocity over all the interfaces.
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      do jj=1,nintf
       do kk=1,nintf

         do  j=0,Nh-1
           do  i=0,N-1
             in=i+2*j+1
 
c                         temp= dcosh(p2*(yct(i)-yct(in)))
c                         $   -dcos(p2*(xct(i)-xct(in))) + del*del
c       
c                 u(i)=u(i)-wt(in)*dsinh(p2*(yct(i)-yct(in)))/temp
c 
c                 v(i)=v(i)+wt(in)*dsin(p2*(xct(i)-xct(in)))/temp

             temp=(yct(i,jj)-yct(in,kk))**2 + 
     &                 (xct(i,jj)-xct(in,kk))**2 + del*del

             u(i,jj)=u(i,jj)-wt(in,kk)*(yct(i,jj)-yct(in,kk))/temp

             v(i,jj)=v(i,jj)+wt(in,kk)*(xct(i,jj)-xct(in,kk))/temp
          end do
         end do
        end do
       end do
 
      do jj=1,nintf
       do i=0,N-1
        u(i,jj)=u(i,jj)*2.d0*h/p2
        v(i,jj)=v(i,jj)*2.d0*h/p2
       end do
       u(N,jj)=u(0,jj)
       v(N,jj)=v(0,jj)
      end do

c      call kfilter(N,dcut,u)
c      call kfilter(N,dcut,v)
      return
      end
 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c  THIS CODE COMPUTES THE NORMAL VELOCITY GIVEN THE U,V FROM THE
c  VELOCITY ROUTINE
c
c  FOR A MATRIX OF VELOCITIES
c
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine rntvelmatr(nintf,m,h,p2,x,y,sl,theta,u,v,un,ut,dcut)
       implicit double precision (a-h,o-z)
       dimension x(0:MAXLEN,10),y(0:MAXLEN,10)
       dimension u(0:MAXLEN,10),v(0:MAXLEN,10)
       dimension un(0:MAXLEN,10),ut(0:MAXLEN,10)
       dimension sx(0:MAXLEN,10),sy(0:MAXLEN,10),theta(0:MAXLEN,10)
       dimension sl(10)

c       call kfilter(m,dcut,u)
c       call kfilter(m,dcut,v)

C       write(22,*)'sx=, sy,sl',sl

       do jj=1,nintf
        do j=0,m-1
         sx(j,jj)=cos(theta(j,jj))
         sy(j,jj)=sin(theta(j,jj))
        end do
       end do


c       call kfilter(m,dcut,sx)
c       call kfilter(m,dcut,sy)
c       call kfilter(m,dcut,u)
c       call kfilter(m,dcut,v)

       do jj=1,nintf
        do j=0,m-1

         un(j,jj)=(sx(j,jj)*v(j,jj)-sy(j,jj)*u(j,jj))
         ut(j,jj)=(sx(j,jj)*u(j,jj)+sy(j,jj)*v(j,jj))

        end do
        un(m,jj)=un(0,jj)
        ut(m,jj)=ut(0,jj)
       end do

c       call kfilter(m,dcut,un)
c       call kfilter(m,dcut,ut)
 
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

