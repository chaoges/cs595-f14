0
5d-4,8192,2.5d-5,1d0,13,2.5d-5,0,0,1,64.d0
2, 0.02, 3, 0.02
0
# set Tau=1.0
#0=no restart, 1=restart
#Tmax, ndim, dt, tau, ncut, tplot, ag, eps, nintf
#  Tmax = max time
#  ndim = number of points on the interface
#  dt   = timestep
#  tau  = surf tension = 1/Ca ?
#  ncut = cut of errors < 10^(-ncut))
#  tplot= time intervals to plot
#  ag   =?
#  eps  =?
#  nintf=1  = number of interfaces
#mode1, eps1, mode2, eps2
#  mode1, mode2 = sine mode, cosine mode number perturbations off a circle
#  eps1, eps2   = amplitude of those perturbations
#massfluxtype
#  0= no flux (massflux =0.0)
#  1= constant massflux = 2pi
#  2= linearly growing massflux = 2pi(1+t) 
  
