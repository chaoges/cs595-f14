ccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
c   THIS FILE COMPUTES AREA FROM MULTIPLE INTERFACE CODE
c
c
c
c
ccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine areacomp(nintf,m,h,p2,sl,theta,areaxy,x0,y0)
       implicit double precision (a-h,o-z)
       dimension sl(10),theta(0:240,10),areaxy(10)
       dimension x(0:240,10),y(0:240,10)
       dimension xs(0:240),ys(0:240)
       dimension avx(10),avy(10),x0(10),y0(10)


c  reconstruct point positions

       call reconmatr(nintf,m,h,p2,sl,theta,x,y,avx,avy,dcut,x0,y0)


c  now do each curve separately

       do jj=1,nintf

        do j=0,m-1
         xs(j)=x(j,jj)
         ys(j)=y(j,jj)
        end do
        xs(m)=xs(0)
        ys(m)=ys(0)

        call area(m,h,p2,dcut,xs,ys,area1)

        areaxy(jj)=area1

       end do



      stop
      end
ccccccccccccccccccccccccccccccccccccccccccccccccccccc
