c
c     XFFT -- wrappers to ancient fft codes FAST/FSST and FFT842
c
#include "defineMaxlen.h"
      subroutine xfftinit(n)
      implicit none
      integer n
      double precision fftwork(1)
      double complex   cfftwork(1), cffttemp(1)
      common /dfftdata/ fftwork
      common /cfftdata/ cfftwork
      common /cffttemp/ cffttemp

      call cffti(n, cfftwork) !initialize
      call rffti(n, fftwork )
      return
      end

      subroutine xcopy( n, a, acopy)
      implicit none
      integer n
      double precision a(0:n), acopy(0:n)
      integer j

      do j=0,n-1
         acopy(j) = a(j) 
      end do
      return
      end

c23456: xmaxerror -- 
      double precision function xmaxerror(n, a, b)
      implicit none
      integer n
      double precision a(*), b(*)

      double precision maxerr
      integer j
      maxerr=0.d0
      do j=1,n
         maxerr = max( maxerr, abs( a(j) - b(j)))
      end do
      xmaxerror =maxerr
      return
      end

c23456: xprint -- show two vectors side by side
      subroutine xprint(ndim,a,b)
      implicit none
      integer ndim
      double precision a(0:ndim+1), b(0:ndim+1)
      integer j

      do j=0, ndim+1
         write(6, 101) j, a(j), b(j)
 101     format( I5,3X,2e25.16)
      end do
      return
      end

c23456: xfast is replacement for FAST, without length limit
      subroutine xfast(b,n)
      implicit none
      double precision b(*)
      integer n
      double precision fftwork(1)
      common /dfftdata/ fftwork
      integer j
c     ..start exec
      call rfftf( n, b, fftwork )
      do j=n+1,3,-1
         b(j)=b(j-1)
      end do
      b(2) = 0.d0
      return
      end

c23456: xfsst is replacement for FSST, without length limit
c       .. added normalization, since FSST returns (1/N) rfftb
      subroutine xfsst(b,n)
      implicit none
      double precision b(*)
      integer n
      double precision fftwork(1)
      common /dfftdata/ fftwork
      integer j
      double precision scale
c     ..start exec
      do j=3,n+2
         b(j-1)=b(j)
      end do
      call rfftb( n, b, fftwork )
      scale = 1.d0/n
      do j=1,n
         b(j) = scale*b(j)
      end do
      b(n+1)=0.d0
      end


      
c     ---------- complex ffts ---------------------------------
      
c23456: xfft842 -- complex fft with cos & sin coeffs separate
      subroutine xfft842( in, n, x,y)
      implicit none
      integer in, n
      double precision x(*), y(*)
      double complex cfftwork(1), cffttemp(1)
      common /cfftdata/ cfftwork
      common /cffttemp/ cffttemp 
      integer j
c     ..begin exec
      if( in.ne.0 ) then
         write( 6,*) "ERROR: XFFT842, only in=0 case defined"
         return
      end if
      do j=1,n
         cffttemp(j) = dcmplx( x(j), y(j) )
      end do

c     dreal(z) and dimag(z)
      call cfftf(n, cffttemp, cfftwork )
      do j=1,n
         x(j) = dreal( cffttemp(j))
         y(j) = dimag( cffttemp(j))
      end do

      return
      end


c       
c       --Mike's Spectral Derivatives code
c
        subroutine fdiff(x,xi,n,h)
        implicit double precision (a-h,o-z)
c        parameter(lda=33000)
        parameter(lda=2*MAXLEN)
        double precision x(*),xi(*) ! changed from x(1), xi(1) **pf
        double precision a(lda),h
        pi=4.d0*datan(1.d0)
        p2=2.d0*pi
        n1=n-1
        n2=n1/2
        do 10 j=1,n1
        a(j)=x(j)
 10     continue
        call xfast(a,n1)
        xi(1)=0.d0
        xi(2)=0.d0
        do 20 j=2,n2+1
        k=2*j-1
        xi(k)=-a(k+1)*dfloat(j-1)
        xi(k+1)=a(k)*dfloat(j-1)
 20     continue
        call xfsst(xi,n1)
        xi(n)=xi(1)
        return
        end

        SUBROUTINE SYNTHCV(Z,ZI,E,N,L)
c        PARAMETER(LDA=33000)
        PARAMETER(LDA=2*MAXLEN)
        COMPLEX*16 Z(N),ZI(L)
        DOUBLE PRECISION E(L)
        COMPLEX*16 CXP1(LDA),CXP2(LDA),U1(LDA),U2(LDA)
        COMPLEX*16 ZJ1,ZJ2
        DOUBLE PRECISION A(LDA),B(LDA)
        N1=N-1
        N2=N1/2
        N21=N2-1
        N22=N2-2
        DO 1 K=1,N1
        A(K)=DREAL(Z(K))/DFLOAT(N1)
        B(K)=DIMAG(Z(K))/DFLOAT(N1)
1       CONTINUE
        CALL XFFT842(0,N1,A,B)
        DO 5 K=1,L
        CXP1(K)=DCMPLX(DCOS(E(K)),DSIN(E(K)))
        CXP2(K)=DCONJG(CXP1(K))
        U1(K)=DCMPLX(A(N2),B(N2))
        U2(K)=DCMPLX(A(N2+2),B(N2+2))
 5      CONTINUE
        DO 10 J=1,N22
        ZJ1=DCMPLX(A(N2-J),B(N2-J))
        ZJ2=DCMPLX(A(N2+J+2),B(N2+J+2))
        DO 10 K=1,L
        U1(K)=ZJ1+CXP1(K)*U1(K)
        U2(K)=ZJ2+CXP2(K)*U2(K)
 10     CONTINUE
        DO 20 K=1,L
        ZI(K)=DCMPLX(A(1),B(1))+DCMPLX(A(N2+1),B(N2+1))*DCOS(N2*E(K))+
     *        CXP1(K)*U1(K)+CXP2(K)*U2(K)
 20     CONTINUE
        RETURN
        END
        subroutine fintg(x,xi,n,h)
        implicit double precision (a-h,o-z)
c        parameter(lda=33000)
        parameter(lda=2*MAXLEN)
        double precision x(*),xi(*)  ! changed from x(1), xi(1) **pf
        double precision a(lda),h
        pi=4.d0*datan(1.d0)
        p2=2.d0*pi
        n1=n-1
        n2=n1/2
        do 10 j=1,n1
        a(j)=x(j)
 10     continue
        call xfast(a,n1)
        xi(1)=0.d0
        xi(2)=0.d0
        do 20 j=2,n2+1
        k=2*j-1
        xi(k)=a(k+1)/(dfloat(j-1))
        xi(k+1)=-a(k)/(dfloat(j-1))
 20     continue
        call xfsst(xi,n1)
        xi(n)=xi(1)
        do 30 j=1,n 
        xi(j)=(a(1)/dfloat(n1))*(j-1)*h+xi(j)-xi(n)
 30     continue
        return
        end
        SUBROUTINE SYNTH(X,XI,E,N,A,B)
        implicit double precision (a-h,o-z)
        double precision X(1),XI
        DOUBLE PRECISION E
        COMPLEX*16 CXP1,CXP2,U1,U2
        COMPLEX*16 ZJ1,ZJ2
        DOUBLE PRECISION A(1),B(1)
        N1=N-1
        N2=N1/2
        N21=N2-1
        N22=N2-2
C        DO 1 K=1,N1
C        A(K)=X(K)/DFLOAT(N1)
C        B(K)=0.D0
C1       CONTINUE
C        CALL FFT842(0,N1,A,B)
        CXP1=DCMPLX(DCOS(E),DSIN(E))
        CXP2=DCONJG(CXP1)
        U1=DCMPLX(A(N2),B(N2))
        U2=DCMPLX(A(N2+2),B(N2+2))
        DO 10 J=1,N22
        ZJ1=DCMPLX(A(N2-J),B(N2-J))
        ZJ2=DCMPLX(A(N2+J+2),B(N2+J+2))
        U1=ZJ1+CXP1*U1
        U2=ZJ2+CXP2*U2
 10     CONTINUE
        XI=A(1)+A(N2+1)*DCOS(N2*E)+DREAL(CXP1*U1+CXP2*U2)
        RETURN
        END

