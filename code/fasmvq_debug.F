c CHANGES: **pf
c -changed maxlen to be a macro. Replaced all 0:9600 w/ 0:MAXLEN
c -must pass through the preprocessor before compiling
c
#include "defineMaxlen.h"

C
C****************************************************************
C
      SUBROUTINE FASMVQ(X,Y,ND,NDMAX,K,RNLX,RNLY,RKAPPA,CX,CY,H,U,W)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER *4 IOUT(2),IERR(10),INFORM(10),ND,NDMAX,K 
c      DOUBLE COMPLEX QA(10000),ZN
      DOUBLE COMPLEX QA(MAXLEN),ZN    ! **pf
      INTEGER *4 WKSPLEN
      PARAMETER ( WKSPLEN=16*MAXLEN+1050 ) ! for DAPIF2 **pf
      DIMENSION RNLX(NDMAX,1),RNLY(NDMAX,1),RKAPPA(NDMAX,1)
      DIMENSION X(NDMAX,1),Y(NDMAX,1)
      DIMENSION U(1),W(1),CX(1),CY(1),H(1)
c      DIMENSION XA(10000),YA(10000),FIELD(2,10000)
c      DIMENSION POTEN(10000),WKSP(620000)
      DIMENSION XA(MAXLEN),YA(MAXLEN),FIELD(2,MAXLEN)   !**pf
      DIMENSION POTEN(MAXLEN),WKSP(WKSPLEN)             !**pf
      common /space/ qa,xa,ya,field,poten,wksp
c      real*4 timep(2), etime

C
C     This subroutine applies the integral operator in a fast
C     manner via the FMM.  It returns the imaginary part of the
C     matrix vector product.
C
C ********************    ON INPUT: ****************************
C
C     K = number of bodies
C     ND = number of points in discretization of each body
C     NDMAX = max number of points in discretization of each body
C     X = x-coordinates of boundary discretization points
C     Y = y-coordinates of boundary discretization points
C     RNLX = array of x-coordinate of normal vector at corresponding
C            point in arrays (X,Y).
C     RNLY = array of y-coordinate of normal vector at corresponding
C            point in arrays (X,Y).
C
C-----------------------------------------------------------
C NOTE: The normal direction is taken to be outward from each body.
C-----------------------------------------------------------
C
C     RKAPPA = curvature at pt (X,Y).
C     CX,CY = coordinates of source point inside Kth body
C     H = mesh width on Kth body
C     U = given vector to which matrix is to be applied
C
C ********************    ON OUTPUT: ****************************
C
C     W = M*U
C
C-----------------------------------------------------------------
C
C    compute complex charge strengths to be used in call to Hilbert
C    matrix and define array of source positions (XA,YA)
C    
      ISTART = 0
      DO 100 NBOD = 1,K
      DO 90 i = 1,ND
         XA(ISTART+I) = X(I,NBOD)
         YA(ISTART+I) = Y(I,NBOD)
         QA(ISTART+I) = 0.0D0
90    CONTINUE
      ISTART = ISTART + ND
100   CONTINUE
C
C     first use odd point charges to compute field at even points.
C
      ISTART = 0
      DO 2100 NBOD = 1,K
      DO 2000 i = 1,ND,2
         ZN = DCMPLX(RNLX(I,NBOD),RNLY(I,NBOD))
         QA(ISTART+I) = ZN*H(NBOD)*U(ISTART+I)
2000  CONTINUE
      ISTART = ISTART+ND
2100  CONTINUE
C
C     set parameters for FMM routine DAPIF2
C
      IOUT(1) = 6
      IOUT(2) = 13
      IFLAG7 = 3
      NAPB = 40
      NINIRE = 2
      TOL = 1.0d-14  ! tolerance, might be too high? **pf
c      TOL = 1.0d-12   ! lowered **pf ...nope.
c      MEX = 38       ! max exponent. WAY OUT OF DATE. Ok for IBM machine **pf
      MEX = MAXEXPONENT( W )  ! PORTABLE -- standard intrinsic, returns max exponent **pf
      EPS7 = 1.0d-14
c      NSP = 620000   ! hardwired magic number, changed **pf
      NSP = WKSPLEN   ! NSP=length of workspace for DAPIF2
      NNN = ND*K

c      print *,'** in fasmvq, MEX=',MEX
c      t0 = etime(timep)
      CALL DAPIF2 (IOUT,IFLAG7,NNN,NAPB,NINIRE,MEX,IERR,INFORM,
     *             TOL,EPS7,XA,YA,QA,POTEN,FIELD,WKSP,NSP,CLOSE)
      print *,'** in FASMVQ: DAPIF2 returned ierr(1)=', IERR(1)
      print *,'**    INFORM[1..4] = ',
     $     INFORM(1), INFORM(2),INFORM(3),INFORM(4)
c      t1 = etime(timep)
      tsec = t1 - t0
c      write(6,*) ' time for dapif2 in fasmvq = ',tsec
c      write(3,*) ' time for dapif2 in fasmvq = ',tsec
C
C     extract imaginary part.
C
      ISTART = 0
      DO 2300 NBOD = 1,K
      DO 2200 I = 2,ND,2
         W(ISTART+I) = FIELD(2,ISTART+I)
2200  CONTINUE
      ISTART = ISTART+ND
2300  CONTINUE
C
C     now use even point charges to compute field at odd points.
C
      ISTART = 0
      DO 2500 NBOD = 1, K
      DO 2400 i = 1,ND
         QA(ISTART+I) = 0.0D0
2400  CONTINUE
      ISTART = ISTART + ND
2500  CONTINUE
C
      ISTART = 0
      DO 3100 NBOD = 1,K
      DO 3000 i = 2,ND,2
         ZN = DCMPLX(RNLX(I,NBOD),RNLY(I,NBOD))
         QA(ISTART+I) = ZN*H(NBOD)*U(ISTART+I)
3000  CONTINUE
      ISTART = ISTART+ND
3100  CONTINUE
C
c      t0 = etime(timep)
c      write(3,3101) (qa(i),i=1,nd*k)
3101  format(/' qa going in to dapif2 = '/(5d12.3))
      CALL DAPIF2 (IOUT,IFLAG7,NNN,NAPB,NINIRE,MEX,IERR,INFORM,
     *             TOL,EPS7,XA,YA,QA,POTEN,FIELD,WKSP,NSP,CLOSE)
      print *,'** in FASMVQ: DAPIF2 returned ierr(1)=', IERR(1)
      print *,'**    INFORM[1..4] = ',
     $     INFORM(1), INFORM(2),INFORM(3),INFORM(4)
c      write(3,3102) (field(2,i),i=1,nd*k)
3102  format(/' imag part of field from dapif2 = '/(5d12.3))
c      t1 = etime(timep)
      tsec = t1 - t0
c      write(6,*) ' time for dapif2 in fasmvq = ',tsec
c      write(3,*) ' time for dapif2 in fasmvq = ',tsec
C
C     extract imaginary part.
C
      ISTART = 0
      DO 3300 NBOD = 1,K
      DO 3200 I = 1,ND,2
         W(ISTART+I) = FIELD(2,ISTART+I)
3200  CONTINUE
      ISTART = ISTART+ND
3300  CONTINUE
C
      RETURN
      END
